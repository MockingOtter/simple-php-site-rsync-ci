# Simple php site Rsync CI

Simple Deployment script for sites over Rsync.
Include linting of "*.php" files

## Getting started

* Put the  .gitlab-ci.yml  at th root of your Gitlab project

* Register the following variables ( You can use those variable globally or per project):
    
    - PHPVERSION : (str) like 8.1 or 7.4 or latest. it will  be used to choose php image for linting.
    - SSH_PRIVATE_KEY : (FILE) ssh key to authenticate on the server
    - LOCALWEBROOT :  (STR) Website root directoey in the repository 
    - EXCLUDED_FILES : (FILE) see rsync exclusion file model
    - USER : (str) ssh username
    - WEB_SERVEUR : (str) Server fqdn or ip.
    - WEBROOT : (str) Website root directory on the server
    - HOMEDIR : (str) Directory wehre  the file "version.txt" will be copied


